package com.neunn.demo.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 服务注册中心eureka
 *
 * @author anyuan
 * @date 2018-12-26 11:06
 */
@SpringBootApplication
@EnableEurekaServer
public class ServiceRegistryCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceRegistryCenterApplication.class, args);
    }
}
