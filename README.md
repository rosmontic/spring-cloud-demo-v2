# spring-cloud-demo

#### 项目地址

[https://gitee.com/vandish/spring-cloud-demo-v2]

#### 介绍
spring-cloud-demo

#### 安装教程

1. 本项目上传时即附带了idea的项目文件，故修改JDK与maven配置后即可以直接下载执行

#### 使用说明

1. 本项目中配置了多个子项目，且部分子项目在启动时需要启动多个节点才能看到效果
2. 项目启动顺序：
    - 服务注册中心：service-registry-center，单节点
    - 配置中心：config-server，可多节点（但没必要）
    - 路由网关：service-zuul，可多节点
    - 分布式跟踪服务：zipkin-server
    - 产品服务：product-service；账户服务：account-service
    - 销售服务：sales-service（因为销售服务中调用到了另外两个服务，所以建议后起）
3. 所有.backup的文件都是无效的备份文件

#### 参与贡献

1. anyuan: 1462958652@qq.com

#### TODO
1. ✔ zuul-rate-limit限流
2. ✔ 将所有配置尽可能的抽取到config-server中
3. ✔ 可以通过bus总线实现配置自动刷新
4. × 可以通过scheduled实现配置定时刷新 [https://blog.csdn.net/u011943534/article/details/80960475]
5. config-server的decrypt
6. ✔ 将zuul的路由相关配置挪到config-server中，并要求能够通过REST刷新配置
7. × 通过git-webhook实现git上的配置文件发生变化时，自动通过POST请求刷新config-client中使用的配置信息

#### WARNING
1. config-client的配置刷新：
  spring-cloud 2.0之后的配置方式改了。首先，@RefreshScope是一直都要用的，而且需要依赖actuator包。然后：
  1.5的配置方式是直接将安全策略关闭，即management.security.enabled=false，然后访问localhost:xxxx/refresh即可；
  2.0的配置方式改为management.endpoints.web.exposure.include=refresh，然后访问地址变为localhost:xxxx/actuator/refresh；
2. 项目中需要用到zuul集群时：
  当项目中需要启用多个zuul节点，且需要多个zuul节点之间使用统一的rate-limit限流规则时，
  可以通过将rate-limit的存储载体由内存中的ConcurrentHashMap改为redis，达到zuul集群间共享一套限流规则的效果。
  详见 [https://ken.io/note/spring-cloud-zuul-ratelimiter-quickstart]
3. 项目中配置多个不同来源的配置中心
  部分场景下，可以使用多个不同来源的配置中心。比如节点从git/svn/database等不同来源获取相同或不同的配置内容。
4. 注意cloud2.0之后的配置变化
  为了使注册时显示真实ip地址，需要添加配置eureka.instance.prefer-ip-address=true和eureka.instance.instance-id=http://${spring.cloud.client.ip-address}:${server.port}
  注意2.0之后获取本机地址的环境变量名称变成了spring.cloud.client.ip-address
5. eureka对失效服务的自动踢出功能[https://www.cnblogs.com/hfultrastrong/p/8623288.html]
  需要在注册中心中启用eureka.server.enable-self-preservation=false，关闭默认的保护机制
  另外还可以在client中设置向server发送心跳检测的时间周期
6.
  