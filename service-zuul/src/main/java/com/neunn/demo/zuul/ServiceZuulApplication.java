package com.neunn.demo.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * zuul服务
 * 本项目中使用了以下组件：
 *  1.config-server
 *      配置内容大部分都放在gitee上，地址为https://gitee.com/vandish/spring-cloud-config
 *  2.config-bus
 *      允许通过POST同名服务中的任意一个节点的/actuator/bus-refresh，达到注册中心中同名服务配置全都刷新的效果
 *  3.rate-limit
 *      通过zuul-rate-limit实现限流的功能，限流相关配置内容都在gitee上
 *
 *
 * @author anyuan
 * @date 2018-12-26 16:17
 */
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@EnableZuulProxy
@RefreshScope
public class ServiceZuulApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceZuulApplication.class, args);
    }
}
