package com.neunn.demo.config.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * /{application}/{profile}[/{label}]
 * /{application}-{profile}.yml
 * /{label}/{application}-{profile}.yml
 * /{application}-{profile}.properties
 * /{label}/{application}-{profile}.properties
 *
 * 可以启动多个config-server节点
 * 启动多个节点的好处，一个是可以防止节点掉线影响业务，其实还可以将多个节点设置成从不同的来源获取配置信息，达到多个节点配置[来源]不同的场景（虽然不建议这么用）
 * @author anyuan
 * @date 2018-12-26 16:51
 */
@SpringBootApplication
@EnableEurekaClient
@EnableConfigServer
public class ConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigServerApplication.class, args);
    }
}
