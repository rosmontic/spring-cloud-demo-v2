package com.neunn.demo.sales.service.fallback;

import com.neunn.demo.sales.service.ProductService;
import org.springframework.stereotype.Component;

/**
 * 自定义一个hystrix类，实现feignClient修饰的接口
 * 并在接口的feignClient中指定fallback类
 * 当出现异常时就会调用指定的fallback类中的实现方法
 *
 * @author anyuan
 * @date 2018-12-26 15:57
 */
@Component
public class ProductServiceFallback implements ProductService {

    @Override
    public String healthFromProductService() {
        return "unable to know if product-service is alive";
    }

    @Override
    public Integer marginFromProductService(String productName) {
        System.out.println("调用marginFromProductService过程中出错，此处将返回一个无效值");
        return -1;
    }
}
