package com.neunn.demo.sales.service;

import com.neunn.demo.sales.service.fallback.ProductServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 通过Feign提供的方式快速创建访问外部rest系统的接口服务
 * FeignClient:
 * name: 标识调用的服务名
 *
 * @author anyuan
 * @date 2018-12-26 15:53
 */
@FeignClient(name = "product-service", fallback = ProductServiceFallback.class)
public interface ProductService {

    @RequestMapping(path = "/health")
    String healthFromProductService();

    /**
     * 无需实现接口，直接调用该方法，运行时将在内存中生成实现类。
     * 可以直接通过该方法调用到product-service服务中的对应接口(/margin)
     *
     * @param productName
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, path = "/margin")
    Integer marginFromProductService(@RequestParam(name = "productName") String productName);
}
