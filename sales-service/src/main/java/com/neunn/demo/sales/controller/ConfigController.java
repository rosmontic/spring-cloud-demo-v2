package com.neunn.demo.sales.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 测试对config-server的调用
 *
 * @author anyuan
 * @date 2018-12-26 17:07
 */
@RefreshScope
@RestController
@RequestMapping(path = "/config")
public class ConfigController {

    /**
     * 参数内容请查看
     * https://gitee.com/vandish/spring-cloud-config/blob/master/configuration/sales-service-dev.yml
     */
    @Value("${project.version}")
    private String projectVersion;
    @Value("${foo}")
    private String foo;

    /**
     * 从config-server中获取指定的配置参数
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, path = "/foo")
    public Map<String, Object> getFoo() {
        Map<String, Object> returnMap = new LinkedHashMap<>();
        returnMap.put("project.version", projectVersion);
        returnMap.put("foo", foo);
        return returnMap;
    }
}
