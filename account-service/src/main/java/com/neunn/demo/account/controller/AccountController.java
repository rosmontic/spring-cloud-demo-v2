package com.neunn.demo.account.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author anyuan
 * @date 2018-12-26 11:01
 */
@RestController
public class AccountController {

    @Value("${server.port}")
    private Integer port;

    @RequestMapping(path = "/health")
    public String health() {
        return "I'm healthy --this message is from port [" + port + "]";
    }

    /**
     * 获取余额
     *
     * @return 余额
     */
    @RequestMapping(method = RequestMethod.GET, path = "/balance")
    public Double balance(@RequestParam(name = "accountName") String accountName) {
        return 500.0;
    }

    /**
     * 扣除余额
     *
     * @return 余额
     */
    @RequestMapping(method = RequestMethod.POST, path = "/spend")
    public Double spend(@RequestParam(name = "accountName") String accountName,
                        @RequestParam(name = "money") Double money) {
        System.out.println("账户[" + accountName + "]花了[" + money + "]元!");
        return balance(accountName) - money;
    }

}
