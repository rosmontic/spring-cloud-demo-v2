package com.neunn.demo.product.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author anyuan
 * @date 2018-12-26 11:13
 */
@RestController
public class ProductController {

    @Value("${server.port}")
    private Integer port;

    @RequestMapping(path = "/health")
    public String health() {
        return "I'm healthy --this message is from port [" + port + "]";
    }

    /**
     * 获取余量
     *
     * @param productName 产品名
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, path = "/margin")
    public Integer margin(@RequestParam(name = "productName") String productName) {
        return 100;
    }

    /**
     * 获取单价
     *
     * @param productName
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, path = "/price")
    public Double price(@RequestParam(name = "productName") String productName) {
        return 7.5;
    }

    /**
     * 减少库存
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, path = "/marginMinus")
    public Integer marginMinus(@RequestParam(name = "productName") String productName,
                               @RequestParam(name = "number") Integer number) {
        System.out.println("[" + productName + "]产品库存减少了[" + number + "]个.");
        return margin(productName) - number;
    }
}
